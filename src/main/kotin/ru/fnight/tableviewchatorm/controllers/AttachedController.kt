package ru.fnight.tableviewchatorm.controllers

import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.css.PseudoClass
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.Alert
import javafx.scene.control.TableColumn
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import javafx.scene.control.cell.ComboBoxTableCell
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.control.cell.TextFieldTableCell
import javafx.util.Callback
import org.hibernate.SessionFactory
import ru.fnight.tableviewchatorm.domains.Attached
import ru.fnight.tableviewchatorm.domains.Message
import java.sql.SQLException
import javax.persistence.PersistenceException

class AttachedController {
    lateinit var attachedTable: TableView<Attached>
    lateinit var attachedUrlColumn: TableColumn<Attached, String>
    lateinit var attachedMessageColumn: TableColumn<Attached, Message>

    lateinit var factory: SessionFactory

    var messages = ArrayList<Message>()

    @FXML
    private fun initialize() {
        attachedTable.isEditable = true

        //url file
        attachedUrlColumn.cellValueFactory = PropertyValueFactory<Attached, String>("fileUrl")
        attachedUrlColumn.cellFactory = TextFieldTableCell.forTableColumn()
        attachedUrlColumn.onEditCommit = EventHandler { event ->
            onChangeUrl(event)
        }

        //message
        attachedMessageColumn.onEditCommit = EventHandler { event ->
            onChangeMessage(event)
        }

        attachedTable.rowFactory = Callback { param ->
            val row = TableRow<Attached>()
            row.itemProperty().addListener { observable, oldValue, newValue ->
                if (newValue != null) {
                    if (newValue.id <= 0) {
                        row.pseudoClassStateChanged(PseudoClass.getPseudoClass("new"), true)
                    } else {
                        row.pseudoClassStateChanged(PseudoClass.getPseudoClass("new"), false)
                    }
                }
            }
            row
        }
    }

    private fun onChangeUrl(event: TableColumn.CellEditEvent<Attached, String>) {
        val attached = event.tableView.items[event.tablePosition.row]
        val oldValue = attached.fileUrl
        attached.fileUrl = event.newValue
        try {
            saveChangedAttached(attached)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            attached.fileUrl = oldValue
            attachedTable.refresh()
        }
    }

    private fun onChangeMessage(event: TableColumn.CellEditEvent<Attached, Message>) {
        val attached = event.tableView.items[event.tablePosition.row]
        val oldValue = attached.message
        attached.message = event.newValue
        try {
            saveChangedAttached(attached)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            attached.message = oldValue
            attachedTable.refresh()
        }
    }

    @Throws(SQLException::class)
    private fun saveChangedAttached(attached: Attached) {
        val session = factory.openSession()
        val transaction = session.beginTransaction()
        session.saveOrUpdate(attached)
        transaction.commit()
        session.close()
        attachedTable.refresh()
    }

    fun reloadAttached(attached: ArrayList<Attached>) {
        val messagesList = FXCollections.observableArrayList(messages)
        attachedMessageColumn.cellValueFactory = Callback { param ->
            val attached = param.value
            SimpleObjectProperty<Message>(attached.message)
        }
        attachedMessageColumn.cellFactory = ComboBoxTableCell.forTableColumn(messagesList)
        attachedTable.items = FXCollections.observableArrayList(attached)
        attachedTable.refresh()
    }

    fun deleteAttached(actionEvent: ActionEvent) {
        val attached = attachedTable.selectionModel.selectedItem ?: return
        if (attached.id > 0) {
            val session = factory.openSession()
            val transaction = session.beginTransaction()
            session.delete(attached)
            try {
                transaction.commit()
            } catch (e: PersistenceException) {
                Alert(Alert.AlertType.ERROR,
                        "Невозможно удалить сообщение, потому что у него есть зависимые объекты").show()
                return
            } finally {
                session.close()
            }
        }
        attachedTable.items.remove(attached)
        attachedTable.refresh()
    }

    fun addNewAttached(actionEvent: ActionEvent) {
        if (messages.count() > 0) {
            val attached = Attached()
            attached.message = messages[0]
            attached.fileUrl = ""
            attachedTable.items.addAll(attached)
            attachedTable.refresh()
        } else {
            Alert(Alert.AlertType.ERROR, "Сначала необходимо добавить сообщения.").show()
        }
    }
}