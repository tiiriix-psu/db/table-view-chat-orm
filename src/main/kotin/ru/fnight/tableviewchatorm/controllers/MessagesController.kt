package ru.fnight.tableviewchatorm.controllers

import javafx.beans.property.SimpleBooleanProperty
import javafx.collections.FXCollections
import javafx.css.PseudoClass
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.control.TableColumn
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import javafx.scene.control.cell.CheckBoxTableCell
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.control.cell.TextFieldTableCell
import javafx.util.Callback
import javafx.util.StringConverter
import org.hibernate.SessionFactory
import org.hibernate.service.ServiceRegistry
import org.postgresql.util.PSQLException
import ru.fnight.tableviewchatorm.domains.Message
import ru.fnight.tableviewchatorm.domains.Session
import java.sql.SQLException
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import javax.persistence.PersistenceException

class MessagesController {
    lateinit var messagesTable: TableView<Message>
    lateinit var messageTextColumn: TableColumn<Message, String>
    lateinit var messageSendColumn: TableColumn<Message, Timestamp>
    lateinit var messageDeleteColumn: TableColumn<Message, Boolean>
    lateinit var messageSessionColumn: TableColumn<Message, Session>

    lateinit var factory: SessionFactory

    var sessions = ArrayList<Session>()

    @FXML
    private fun initialize() {
        messagesTable.isEditable = true

        //text
        messageTextColumn.cellValueFactory = PropertyValueFactory<Message, String>("text")
        messageTextColumn.cellFactory = TextFieldTableCell.forTableColumn()
        messageTextColumn.onEditCommit = EventHandler { event ->
            onChangeText(event)
        }

        //send
        messageSendColumn.cellValueFactory = PropertyValueFactory<Message, Timestamp>("send")
        messageSendColumn.cellFactory = TextFieldTableCell.forTableColumn<Message, Timestamp>(
                object : StringConverter<Timestamp>() {
                    override fun toString(obj: Timestamp): String {
                        return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(obj)
                    }

                    override fun fromString(string: String): Timestamp {
                        return try {
                            Timestamp.valueOf(string)
                        } catch (e: IllegalArgumentException) {
                            Alert(Alert.AlertType.ERROR, "Неверный формат даты").show()
                            Timestamp.valueOf(LocalDateTime.now())
                        }
                    }
                }
        )
        messageSendColumn.onEditCommit = EventHandler { event ->
            onChangeSend(event)
        }

        //deleted
        messageDeleteColumn.cellValueFactory = Callback { param ->
            val message = param.value
            val property = SimpleBooleanProperty(message.deleted ?: false)
            property.addListener { _, _, newValue ->
                message.deleted = newValue
                saveChangedMessage(message)
            }
            property
        }
        messageDeleteColumn.cellFactory = Callback {
            val cell = CheckBoxTableCell<Message, Boolean>()
            cell.alignment = Pos.CENTER
            cell
        }

        messageSessionColumn.cellValueFactory = PropertyValueFactory<Message, Session>("session")

        messagesTable.rowFactory = Callback { param ->
            val row = TableRow<Message>()
            row.itemProperty().addListener { observable, oldValue, newValue ->
                if (newValue != null) {
                    if (newValue.id <= 0) {
                        row.pseudoClassStateChanged(PseudoClass.getPseudoClass("new"), true)
                    } else {
                        row.pseudoClassStateChanged(PseudoClass.getPseudoClass("new"), false)
                    }
                }
            }
            row
        }
    }

    private fun onChangeSend(event: TableColumn.CellEditEvent<Message, Timestamp>) {
        val message = event.tableView.items[event.tablePosition.row]
        val oldValue = message.send
        message.send = event.newValue
        try {
            saveChangedMessage(message)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            message.send = oldValue
            messagesTable.refresh()
        }
    }

    private fun onChangeText(event: TableColumn.CellEditEvent<Message, String>) {
        val message = event.tableView.items[event.tablePosition.row]
        val oldValue = message.text
        val newValue = event.newValue.trim()
        if (newValue.isBlank()) {
            Alert(Alert.AlertType.ERROR, "Невозможно сохранить сообщение с пустым текстом").show()
            message.text = oldValue
            messagesTable.refresh()
            return
        }
        message.text = newValue
        try {
            saveChangedMessage(message)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            message.text = oldValue
            messagesTable.refresh()
        }
    }

    @Throws(SQLException::class)
    private fun saveChangedMessage(message: Message) {
        val session = factory.openSession()
        val transaction = session.beginTransaction()
        session.saveOrUpdate(message)
        transaction.commit()
        session.close()
        messagesTable.refresh()
    }

    fun reloadMessages(messages: ArrayList<Message>) {
        messagesTable.items = FXCollections.observableArrayList(messages)
        messagesTable.refresh()
    }

    fun deleteMessage(actionEvent: ActionEvent) {
        val message = messagesTable.selectionModel.selectedItem ?: return
        if (message.id > 0) {
            val session = factory.openSession()
            val transaction = session.beginTransaction()
            session.delete(message)
            try {
                transaction.commit()
            } catch (e: PersistenceException) {
                Alert(Alert.AlertType.ERROR,
                        "Невозможно удалить сообщение, потому что у него есть зависимые объекты").show()
                return
            } finally {
                session.close()
            }
        }
        messagesTable.items.remove(message)
        messagesTable.refresh()
    }

    fun addNewMessage(actionEvent: ActionEvent) {
        if (sessions.size > 0) {
            val message = Message()
            message.send = Timestamp.valueOf(LocalDateTime.now())
            message.text = ""
            message.deleted = false
            message.session = sessions[0]
            messagesTable.items.add(message)
            messagesTable.refresh()
        } else {
            Alert(Alert.AlertType.ERROR, "Сначала необходимо добавить сессии.").show()
        }
    }


}