package ru.fnight.tableviewchatorm.controllers

import javafx.fxml.FXML
import javafx.scene.control.TabPane
import javafx.scene.layout.AnchorPane
import org.hibernate.SessionFactory
import org.hibernate.boot.registry.StandardServiceRegistryBuilder
import org.hibernate.cfg.Configuration
import org.hibernate.service.ServiceRegistry
import ru.fnight.tableviewchatorm.domains.Attached
import ru.fnight.tableviewchatorm.domains.Message
import ru.fnight.tableviewchatorm.domains.Session
import java.util.ArrayList


class Window {

    @FXML
    lateinit var tabPane: TabPane
    @FXML
    lateinit var messagesTab: AnchorPane
    @FXML
    lateinit var messagesTabController: MessagesController
    @FXML
    lateinit var attachedTab: AnchorPane
    @FXML
    lateinit var attachedTabController: AttachedController


    lateinit var factory: SessionFactory
    lateinit var service: ServiceRegistry

    private val messages = ArrayList<Message>()
    private val sessions = ArrayList<Session>()

    @FXML
    fun initialize() {
        val config = Configuration()
        config.configure()
        config.addAnnotatedClass(Message::class.java)
        service = StandardServiceRegistryBuilder().applySettings(config.properties).build()
        factory = config.buildSessionFactory()

        messagesTabController.factory = factory
        attachedTabController.factory = factory

        attachedTabController.messages = messages

        messagesTabController.sessions = sessions

        tabPane.selectionModel.selectedItemProperty().addListener { _, _, _ ->
            val selectedTabName = tabPane.selectionModel.selectedItem.text
            when (selectedTabName) {
                "Сообщения" -> {
                    reloadMessages()
                }
                "Прикрепленные" -> {
                    reloadMessages()
                    reloadAttached()
                }
            }
        }
        reloadMessages()
    }

    private fun reloadMessages() {
        val session = factory.openSession()
        val transaction = session.beginTransaction()
        messages.clear()
        messages.addAll(session.createQuery("from Message").list().map { any -> any as Message })
        sessions.clear()
        sessions.addAll(session.createQuery("from Session").list().map { any -> any as Session})
        transaction.commit()
        session.close()
        messagesTabController.reloadMessages(messages)
    }

    private fun reloadAttached() {
        val session = factory.openSession()
        val transaction = session.beginTransaction()
        val attached = ArrayList(session.createQuery("from Attached").list().map { any -> any as Attached })
        transaction.commit()
        session.close()
        attachedTabController.reloadAttached(attached)
    }
}