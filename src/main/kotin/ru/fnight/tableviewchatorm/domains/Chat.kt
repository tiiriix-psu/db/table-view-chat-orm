package ru.fnight.tableviewchatorm.domains

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
class Chat {

    @Id
    @Column(nullable = false, unique = true, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Basic
    @Column(nullable = false, length = 200)
    var name: String = ""

    @Basic
    @Column(nullable = false)
    var creation: Timestamp = Timestamp.valueOf(LocalDateTime.now())

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", referencedColumnName = "id", nullable = false)
    var owner: User? = null

    @Basic
    @Column(nullable = true)
    var deleted: Boolean? = null

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val chat = o as Chat?
        return id == chat!!.id &&
                name == chat.name &&
                creation == chat.creation &&
                owner == chat.owner &&
                deleted == chat.deleted
    }

    override fun hashCode(): Int {
        return Objects.hash(id, name, creation, owner, deleted)
    }

    override fun toString(): String {
        return name
    }
}