package ru.fnight.tableviewchatorm.domains

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity(name = "users")
class User {

    @Id
    @Column(nullable = false, unique = true, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Basic
    @Column(nullable = false, length = 100)
    var login: String = ""

    @Basic
    @Column(nullable = false)
    var registration: Timestamp = Timestamp.valueOf(LocalDateTime.now())

    @Basic
    @Column(nullable = false)
    var pwd: String = ""

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val user = o as User?
        return id == user!!.id &&
                login == user.login &&
                registration == user.registration &&
                pwd == user.pwd
    }

    override fun hashCode(): Int {
        return Objects.hash(id, login, registration, pwd)
    }

    override fun toString(): String {
        return login
    }
}