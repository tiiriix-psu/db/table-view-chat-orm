package ru.fnight.tableviewchatorm.domains

import java.util.Objects
import javax.persistence.*

@Entity
class Attached {

    @Id
    @Column(nullable = false, unique = true, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Basic
    @Column(name = "file_url", nullable = false, length = 200)
    var fileUrl: String? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "message_id", referencedColumnName = "id", nullable = false)
    var message: Message? = null

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val attached = o as Attached?
        return id == attached!!.id && fileUrl == attached.fileUrl
    }

    override fun hashCode(): Int {
        return Objects.hash(id, fileUrl)
    }
}
