package ru.fnight.tableviewchatorm.domains

import java.sql.Timestamp
import java.util.Objects
import javax.persistence.*

@Entity
class Message {

    @Id
    @Column(nullable = false, unique = true, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Basic
    @Column(nullable = true, length = -1)
    var text: String? = null

    @Basic
    @Column(nullable = false)
    var send: Timestamp? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "session_id", referencedColumnName = "id", nullable = false)
    var session: Session? = null

    @Basic
    @Column(nullable = true)
    var deleted: Boolean? = null

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val message = o as Message?
        return id == message!!.id &&
                text == message.text &&
                send == message.send &&
                deleted == message.deleted
    }

    override fun hashCode(): Int {
        return Objects.hash(id, text, send, deleted)
    }

    override fun toString(): String {
        return text?:""
    }


}
