package ru.fnight.tableviewchatorm.domains

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
class Session {

    @Id
    @Column(nullable = false, unique = true, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chat_id", referencedColumnName = "id", nullable = false)
    var chat: Chat? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    var user: User? = null

    @Basic
    @Column(nullable = false)
    var start: Timestamp = Timestamp.valueOf(LocalDateTime.now())

    @Basic
    @Column(nullable = true)
    var end: Timestamp? = null

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val session = o as Session?
        return id == session!!.id &&
                chat == session.chat &&
                user == session.user &&
                start == session.start &&
                end == session.end
    }

    override fun hashCode(): Int {
        return Objects.hash(id, chat, user, start, end)
    }

    override fun toString(): String {
        return "$user - $chat"
    }

}